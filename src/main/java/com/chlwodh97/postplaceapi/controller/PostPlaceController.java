package com.chlwodh97.postplaceapi.controller;


import com.chlwodh97.postplaceapi.model.PostPlaceChangeInfoRequest;
import com.chlwodh97.postplaceapi.model.PostPlaceItem;
import com.chlwodh97.postplaceapi.model.PostPlaceRequest;
import com.chlwodh97.postplaceapi.model.PostPlaceResponse;
import com.chlwodh97.postplaceapi.service.PostPlaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/post-place")
public class PostPlaceController {
    private final PostPlaceService postPlaceService;

    @PostMapping("/new")
    public String setPostPlace(@RequestBody PostPlaceRequest request){
        postPlaceService.setPostPlace(request);

        return "게시글 등록되었습니다.";
    }

    @GetMapping("/list")
    public List<PostPlaceItem> getPostPlaces() {

        return postPlaceService.getPostPlaces();

    }

    @GetMapping("/datail/{id}")
    public PostPlaceResponse getPostPlace(@PathVariable long id) {

        return postPlaceService.getPostPlace(id);
    }

    @PutMapping("change/{id}")
    public String putPostPlace(@PathVariable long id , @RequestBody PostPlaceChangeInfoRequest request){
        postPlaceService.putPostPlace(id, request);

        return "게시글  수정되었습니다";
    }

    @DeleteMapping("/del/{id}")
    public String delPostPlace(@PathVariable long id){
        postPlaceService.delPostPlace(id);

        return "게시글 삭제 되었습니다";
    }
}
