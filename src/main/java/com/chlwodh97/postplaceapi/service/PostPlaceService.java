package com.chlwodh97.postplaceapi.service;


import com.chlwodh97.postplaceapi.entity.PostPlace;
import com.chlwodh97.postplaceapi.model.PostPlaceChangeInfoRequest;
import com.chlwodh97.postplaceapi.model.PostPlaceItem;
import com.chlwodh97.postplaceapi.model.PostPlaceRequest;
import com.chlwodh97.postplaceapi.model.PostPlaceResponse;
import com.chlwodh97.postplaceapi.repository.PostPlaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostPlaceService {
    private final PostPlaceRepository postPlaceRepository;

    public void setPostPlace(PostPlaceRequest request){

        PostPlace addData = new PostPlace();
        addData.setTitle(request.getTitle());
        addData.setName(request.getName());
        addData.setContent(request.getContent());
        addData.setPostDate(LocalDate.now());

        postPlaceRepository.save(addData);
    }


    public List<PostPlaceItem> getPostPlaces() {
        //정보 다가져
        List<PostPlace> originList = postPlaceRepository.findAll();

        List<PostPlaceItem> viewList = new LinkedList<>();

        for (PostPlace postPlace : originList){
            PostPlaceItem addItem = new PostPlaceItem();
            addItem.setId(postPlace.getId());
            addItem.setTitle(postPlace.getTitle());
            addItem.setName(postPlace.getName());
            addItem.setPostDate(LocalDate.now());

            viewList.add(addItem);

        }
        return viewList;
    }

    public PostPlaceResponse getPostPlace(long id) {
        PostPlace originData = postPlaceRepository.findById(id).orElseThrow();

        PostPlaceResponse response = new PostPlaceResponse();
        response.setId(originData.getId());
        response.setTitle(originData.getTitle());
        response.setName(originData.getName());
        response.setContent(originData.getContent());
        response.setPostDate(LocalDate.now());

        return response;

    }

    public void putPostPlace(long id , PostPlaceChangeInfoRequest postPlaceChangeInfoRequest) {
        PostPlace putData = postPlaceRepository.findById(id).orElseThrow();

        putData.setTitle(postPlaceChangeInfoRequest.getTitle());
        putData.setName(postPlaceChangeInfoRequest.getName());
        putData.setContent(postPlaceChangeInfoRequest.getContent());

        postPlaceRepository.save(putData);
    }

    public void delPostPlace(long id){
        postPlaceRepository.deleteById(id);
    }

}
