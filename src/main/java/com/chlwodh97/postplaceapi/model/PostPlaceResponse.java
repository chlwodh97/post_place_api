package com.chlwodh97.postplaceapi.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PostPlaceResponse {
    private long id;
    private String name;
    private String title;
    private String content;
    private LocalDate postDate;
}
