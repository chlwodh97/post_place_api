package com.chlwodh97.postplaceapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PostPlaceItem {
    private long id;
    private String title;
    private String name;
    private LocalDate postDate;
}
