package com.chlwodh97.postplaceapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostPlaceChangeInfoRequest {
    private String name;
    private String title;
    private String content;
}
