package com.chlwodh97.postplaceapi.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PostPlaceRequest {
    private String title;
    private String name;
    private String content;
    private LocalDate postDate;
}
