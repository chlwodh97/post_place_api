package com.chlwodh97.postplaceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostPlaceApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostPlaceApiApplication.class, args);
    }

}
