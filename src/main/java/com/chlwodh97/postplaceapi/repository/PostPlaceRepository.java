package com.chlwodh97.postplaceapi.repository;

import com.chlwodh97.postplaceapi.entity.PostPlace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostPlaceRepository extends JpaRepository<PostPlace , Long> {
}
